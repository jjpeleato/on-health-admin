# Admin: On Health

https://onhealth-admin.alphamega.es/

## Project

API Platform Admin is a tool to automatically create a fancy (Material Design) and fully featured administration interface for any API supporting the Hydra Core Vocabulary, including but not limited to all APIs created using the API Platform framework.

The generated administration is a 100% standalone Single-Page-Application with no coupling to the server part, according to the API-first paradigm.

API Platform Admin parses the Hydra documentation then uses the awesome React Admin library (and React) to expose a nice, responsive, management interface (Create-Retrieve-Update-Delete) for all available resources.

### Installing dependencies

You must install the environment manually: Node.JS and Yarn.

For more information visit:

- Node and NPM: https://nodejs.org/es/
- Yarn: https://yarnpkg.com/es-ES/

**Note:** If you work with Windows. To execute the commands, we recommend installing **Cygwin** http://www.cygwin.com/

**Note:** I recommend installing the following IDE for PHP Programming: Visual Studio Code (https://code.visualstudio.com/) or PHPStorm (recommended) (https://www.jetbrains.com/phpstorm/).

### Project skeleton

```
├─ public/ # Public directory.
├─ src/ # Source directory.
├─ .editorconfig
├─ .env.development.local.dist # Environment Variables
├─ .env.production.local.dist # Environment Variables
├─ .gitignore
├─ .htaccess
├─ deploy.sh.dist # Shell Script for deploy to environments
├─ package.json
└─ README.md
```

### Installing

1. Copy `.env.development.local.dist` to `.env.development.local` and edit the local credentials. Example:
    - `REACT_APP_API_PROTOCOL=http`
    - `REACT_APP_API_DOMAIN=web.onhealth.lndo.site:8000`
    - `REACT_APP_API_BASE=api`
    - `REACT_APP_API_ENTRYPOINT=$REACT_APP_API_PROTOCOL://$REACT_APP_API_DOMAIN`
    - `REACT_APP_API_URL=$REACT_APP_API_PROTOCOL://$REACT_APP_API_DOMAIN/$REACT_APP_API_BASE`
    - `REACT_APP_API_LOGIN=`
    - `REACT_APP_API_DEV=`
2. If required. Copy `.env.production.local.dist` to `.env.production.local` and edit the production credentials. Example:
    - `REACT_APP_API_PROTOCOL=https`
    - `REACT_APP_API_DOMAIN=onhealth-api.alphamega.es`
    - `REACT_APP_API_BASE=api`
    - `REACT_APP_API_ENTRYPOINT=$REACT_APP_API_PROTOCOL://$REACT_APP_API_DOMAIN`
    - `REACT_APP_API_URL=$REACT_APP_API_PROTOCOL://$REACT_APP_API_DOMAIN/$REACT_APP_API_BASE`
    - `REACT_APP_API_LOGIN=`
    - `REACT_APP_API_DEV=`
3. Copy `deploy.sh.dist` to `deploy.sh` and edit the credentials according repository and SSH data.
    - `SSH_DIRECTORY`
4. Run `$yarn install`
5. End. Happy developing.

### Developing

- Yarn actions commands list:
    - `$yarn start`
        - Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser. The page will reload if you make edits. You will also see any lint errors in the console.
    - `$yarn test`
        - Launches the test runner in the interactive watch mode.
    - `$yarn build:pro`
        - Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance. The build is minified and the filenames include the hashes. Your app is ready to be deployed!
    - `$yarn build:dev`
        - Builds the app for development to the `build` folder.

### Deploy to environments

The present project uses a bash file called `deploy.sh`. Execute all commands to deploy in any environment. You only have to modify the git repository and server data.
1. Run `$lando ssh` then: `$sh deploy.sh`.

### Technologies and tools

The present project uses several technologies and tools for the automation and development process. For more information and learning visit the following links.

1. API Platform: https://api-platform.com/
2. React: https://reactjs.org/
3. React admin: https://marmelab.com/react-admin/
4. Git: https://git-scm.com/
5. NPM: https://www.npmjs.com/
6. Yarn: https://yarnpkg.com/
6. Webpack: https://webpack.js.org/


**Note:** Thanks a lot of developers that to work on this projects.

## Finally

**Remove all themes or plugins that you don't use.**

More information on the following commits. If required.

Grettings **@jjpeleato**.
